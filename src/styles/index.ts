import spacingStyling from "./SpacingStyling";
import flexStyling from './FlexStyling';
import generalStyling from './GeneralStyling';

export {
    spacingStyling,
    flexStyling,
    generalStyling
}