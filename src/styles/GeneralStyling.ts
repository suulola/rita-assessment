
import { StyleSheet } from 'react-native';
import { APP_BACKGROUND_GRAY, APP_GREEN } from '../utils/constants';

const generalStyling = StyleSheet.create({
    container: {
        backgroundColor: APP_BACKGROUND_GRAY,
    },
    textButton: {
        backgroundColor: "transparent",
        height: 30,
        width: 170,
    },
    textButtonText: {
        color: APP_GREEN,
        fontFamily: "SFProDisplay-Regular",
        textDecorationLine: "underline"
    }
});

export default generalStyling