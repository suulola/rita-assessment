
import { StyleSheet } from 'react-native';

const spacingStyling = StyleSheet.create({
    pt_20: {
        paddingTop: 50,
    },
    pb_10: {
        paddingBottom: 20,
    },
    mt_20: {
        marginTop: 30,
    },
    mt_10: {
        marginTop: 10,
    },
    mb_10: {
        marginBottom: 10,
    },
    mb_20: {
        marginBottom: 30,
    },
});

export default spacingStyling;