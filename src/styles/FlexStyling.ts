
import { StyleSheet } from 'react-native';

const flexStyling = StyleSheet.create({
    flex_1: {
        flex: 1,
    },
    centralizedRowContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    row: {
        flexDirection: "row",
    },
    rowBetween: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    columnBetween: {
        justifyContent: "space-between"
    },
    columnCenter: {
        alignItems: "center"
    },
    alignEnd: {
        alignItems: "flex-end"
    },
    
});

export default flexStyling;