export const APP_TEXT_BLACK = "#2D2D2D"

export const APP_TITLE = "#0e1427";
export const APP_BLACK = "#000000";
export const APP_TEXT_GRAY = "#8F9BB3"; 

export const APP_BUTTON_GRAY = "#C8C8C8"; 
export const APP_BACKGROUND_GRAY = "#fff5dd"; 
export const APP_BORDER_GRAY = "#cecece"; 
export const APP_GREEN = "#0dcc9c"; 
