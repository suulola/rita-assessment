import React from 'react'
import { ActivityIndicator, View } from 'react-native'
import { flexStyling, spacingStyling } from '../styles';
import { APP_GREEN } from '../utils/constants';

interface ILoader {
    size?: number | "large" | "small" | undefined
}

const Loader = ({ size }: ILoader) => {
    const { centralizedRowContainer } = flexStyling
    const { mt_10 } = spacingStyling;
    return (
        <View style={[mt_10, centralizedRowContainer]}>
            <ActivityIndicator
                size={size ?? "small"}
                color={APP_GREEN}
            />
        </View>
    )
}

export default Loader;