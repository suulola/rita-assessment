import React from 'react'
import { TouchableOpacity, StyleSheet, Image, GestureResponderEvent, ImageSourcePropType, StyleProp, ImageStyle, ViewStyle, TextStyle } from 'react-native';
import { APP_BORDER_GRAY, APP_BUTTON_GRAY, APP_GREEN } from '../utils/constants';
import CustomText from './CustomText';
import Loader from './Loader';

type LoaderSize = "large" | "small";

interface ICustomButton {
  title: string,
  onPress: ((event: GestureResponderEvent) => void),
  loading?: boolean,
  disabled?: boolean,
  containerStyle?: StyleProp<ViewStyle>,
  textStyle?: StyleProp<TextStyle>,
  imageSource?: ImageSourcePropType,
  imageStyle?: StyleProp<ImageStyle>,
  loaderSize?: LoaderSize,
}

const CustomButton = ({
  title, onPress, textStyle,
  containerStyle, loading,
  imageSource, imageStyle, disabled,
  loaderSize,
}: ICustomButton) => {
  return (
    <>
      {
        loading ?
          <Loader size={loaderSize ?? "large"} />
          :
          <TouchableOpacity
            onPress={onPress}
            disabled={disabled}
            style={[styles.button, disabled === true ? styles.disabled : styles.active, containerStyle]}>
            {imageSource && <Image
              source={imageSource}
              style={[styles.otherImg, imageStyle]}
              resizeMode="contain"
            />}
            <CustomText text={title} otherStyles={[styles.text, textStyle]} />
          </TouchableOpacity>
      }
    </>
  )
}

const styles = StyleSheet.create({
  button: {
    height: 50,
    minHeight: 22,
    borderRadius: 12,
    borderWidth: 1,
    borderColor: APP_BORDER_GRAY,
    marginTop: 15,
    width: "88%",
    flexDirection: "row",
    justifyContent: "center",
    alignItems: "center",
  },
  disabled: {
    backgroundColor: APP_BUTTON_GRAY,
  },
  active: {
    backgroundColor: 'transparent',
  },
  otherImg: {
    width: 20,
    height: 15,
    marginHorizontal: 6,
  },
  text: {
    color: APP_GREEN,
    fontFamily: "SFProDisplay-Bold"
  }
})

export default CustomButton
