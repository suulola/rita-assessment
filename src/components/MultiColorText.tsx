import React from 'react'
import { StyleSheet, View } from 'react-native'
import { flexStyling, spacingStyling, generalStyling } from '../styles'
import CustomButton from './CustomButton'
import CustomText from './CustomText'
import { APP_GREEN } from '../utils/constants';

interface IMultiple {
    onPress : () => void, 
    displayText : string, 
    loading?: boolean, 
    actionText: string, 
    width?: string | number,
}

const MultiColorText = ({ onPress, displayText, loading, actionText, width }: IMultiple) => {

    const { row, centralizedRowContainer } = flexStyling
    const { pb_10, mt_10 } = spacingStyling
    const { textButton, textButtonText } = generalStyling

    return (
        <View style={[row, centralizedRowContainer,mt_10, pb_10]}>
            <CustomText
                text={displayText}
                otherStyles={styles.text}
                type="subTitle"
            />
            <CustomButton
                loaderSize="small"
                loading={loading}
                textStyle={textButtonText}
                containerStyle={[textButton, { marginTop: 0, borderWidth: 0, width: width ?? 70 }]}
                title={actionText}
                onPress={onPress}
            />
        </View>
    )
}

export default MultiColorText

const styles = StyleSheet.create({
    text: {
        color: APP_GREEN
    }
})
