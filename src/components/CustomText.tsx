import React from 'react'
import { Text, StyleSheet, StyleProp, TextStyle } from 'react-native'
import { APP_TITLE, APP_TEXT_GRAY } from '../utils/constants';

type StatusTypes = "title" | "subTitle" | "error" | undefined;

interface ICustomText {
  type?: StatusTypes,
  text: string,
  left?: boolean,
  otherStyles?: StyleProp<TextStyle>
}

const CustomText = ({ type, text, otherStyles, left }: ICustomText) => {
  return (
    <Text style={[type === "title" ? styles.title : type === "error" ? styles.error : styles.subTitle, { textAlign: left ? "left" : "center" }, otherStyles]}>
      {text}
    </Text>
  )
}

const styles = StyleSheet.create({
  title: {
    fontSize: 27,
    fontFamily: "SFProDisplay-Medium",
    color: APP_TITLE,
    textShadowColor: APP_TEXT_GRAY,
    textShadowOffset: { width: 1, height: 1 },
    textShadowRadius: 1,
    marginBottom: 5,
  },
  subTitle: {
    fontSize: 15,
    fontFamily: "SFProDisplay-Regular",
    color: APP_TEXT_GRAY,
  },
  error: {
    fontSize: 15,
    lineHeight: 16,
    color: "#c54444",
    marginVertical: 10,
    fontFamily: "SFProDisplay-Regular"
  },
})

export default CustomText
