import React, { useState } from 'react'
import { StyleSheet, SafeAreaView, View } from 'react-native'
import CustomButton from '../components/CustomButton'
import CustomText from '../components/CustomText'
import MultiColorText from '../components/MultiColorText'
import { flexStyling, spacingStyling } from '../styles'
import { wait } from '../utils/helper';

const SignUp = () => {

    const [loadingAndError, setLoadingAndError] = useState({ loading: false, error: "" })

    const handleGoogleSignIn = () => {
        setLoadingAndError(oldState => ({ ...oldState, loading: true }))
        wait(2000).then(() => {
            setLoadingAndError(oldState => ({ ...oldState, loading: false }))
        })
    }
    const handleAppleSignIn = () => { }
    const navigateToLogin = () => { }

    // STYLING
    const { flex_1, centralizedRowContainer } = flexStyling
    const { mb_10, mt_20, pt_20 } = spacingStyling

    return (
        <SafeAreaView style={[flex_1, pt_20, centralizedRowContainer]}>
            <CustomText text="Sign Up" type="title" />
            {loadingAndError.error !== "" && <CustomText text={loadingAndError.error} type="error" />}
            <CustomText text="Create an account by choosing an option below:" />
            <CustomButton
                textStyle={styles.imageButton}
                imageSource={require("../assets/images/google_icon.png")}
                title="SIGN UP WITH GOOGLE"
                loading={loadingAndError.loading}
                onPress={handleGoogleSignIn} />
            <CustomButton
                textStyle={styles.imageButton}
                imageSource={require("../assets/images/apple.png")}
                title="SIGN UP WITH APPLE"
                onPress={handleAppleSignIn} />
            <CustomText
                text="OR"
                otherStyles={[mt_20, mb_10]}
            />
            <CustomButton
                title="SIGN UP WITH EMAIL"
                onPress={handleAppleSignIn} />
            <MultiColorText
                onPress={navigateToLogin}
                displayText={"Already have an account?"}
                actionText={"Log in"}
                width={50}
            />
        </SafeAreaView>
    )
}

export default SignUp

const styles = StyleSheet.create({
    imageButton: {
        textAlign: "left",
        width: 180
    }
})
